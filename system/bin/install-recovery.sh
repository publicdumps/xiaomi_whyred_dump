#!/system/bin/sh
if ! applypatch -c EMMC:/dev/block/bootdevice/by-name/recovery:33895758:6e2239d80aba1dbd5f81c776a8167532922f2450; then
  applypatch -b /system/etc/recovery-resource.dat EMMC:/dev/block/bootdevice/by-name/boot:27739466:5f37e3867e6710881014401d0fd396104cc90302 EMMC:/dev/block/bootdevice/by-name/recovery 6e2239d80aba1dbd5f81c776a8167532922f2450 33895758 5f37e3867e6710881014401d0fd396104cc90302:/system/recovery-from-boot.p && log -t recovery "Installing new recovery image: succeeded" || log -t recovery "Installing new recovery image: failed"
else
  log -t recovery "Recovery image already installed"
fi
