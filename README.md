## whyred-user 9 PKQ1.180904.001 V11.0.1.0.PEIRUXM release-keys
- Manufacturer: xiaomi
- Platform: sdm660
- Codename: whyred
- Brand: xiaomi
- Flavor: whyred-user
- Release Version: 9
- Id: PKQ1.180904.001
- Incremental: V11.0.1.0.PEIRUXM
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: false
- Locale: en-GB
- Screen Density: 440
- Fingerprint: xiaomi/whyred_ru/whyred:9/PKQ1.180904.001/V11.0.1.0.PEIRUXM:user/release-keys
- OTA version: 
- Branch: whyred-user-9-PKQ1.180904.001-V11.0.1.0.PEIRUXM-release-keys
- Repo: xiaomi_whyred_dump


>Dumped by [Phoenix Firmware Dumper](https://github.com/DroidDumps/phoenix_firmware_dumper)
